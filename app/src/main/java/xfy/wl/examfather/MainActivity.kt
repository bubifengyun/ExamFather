package xfy.wl.examfather

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import io.github.kbiakov.codeview.classifier.CodeProcessor
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.noButton
import org.jetbrains.anko.toast
import org.jetbrains.anko.yesButton
import org.scilab.forge.jlatexmath.core.AjLatexMath
import xfy.wl.examfather.storage.AppPreferences


class MainActivity : AppCompatActivity() {

    private lateinit var examDBHelper: ExamDBHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        AjLatexMath.init(this)
        CodeProcessor.init(this)

        handlePermisson()

        examDBHelper = ExamDBHelper.getInstance(this)
        _tv_top_score = tv_top_score
        val preferences = AppPreferences(this)
        setTopScore(preferences.getTopScore())

        button_new_game.setOnClickListener(this::onClickNewGame)
        button_error_history.setOnClickListener(this::onClickErrorHistory)
        button_law_sets.setOnClickListener(this::onClickLawSets)
        button_clear_score.setOnClickListener(this::onClickClearScore)
        button_exit.setOnClickListener(this::onClickExit)
    }

    private fun onClickNewGame(view: View) {
        initData()
    }

    private fun initData(tableName: String = ExamDBHelper.TABLE_NAME,
                         condition: String = "1=1 order by random() limit 100",
                         intentExtra: Map<String, Boolean> = mapOf(GameActivity.IS_EXAM to true)) {
        Thread(Runnable {
//            this@MainActivity.runOnUiThread(Runnable {
//                progressBar.visibility = View.VISIBLE
//            })

            try {
                ExamDBHelper.TABLE_NAME = tableName
                ExamDBHelper.examInfoList = examDBHelper.query(condition)
                var _intent = Intent(this, GameActivity::class.java)
                intentExtra.forEach{
                    key, value -> _intent.putExtra(key, value)
                }
                startActivity(_intent)
            } catch (e: Exception) {
                e.printStackTrace()
            }

//            this@MainActivity.runOnUiThread(Runnable {
//                progressBar.visibility = View.GONE
//            })
        }).start()
    }

    private fun onClickErrorHistory(view: View) {
        TODO("尚未完成")
    }

    private fun onClickLawSets(view: View) {
        var _intentExtra = mapOf(GameActivity.IS_EXAM to false)
        initData(condition = "1=1", intentExtra = _intentExtra)
    }

    private fun onClickClearScore(view: View) {
        val preferences = AppPreferences(this)
        preferences.clearTopScore()
        tv_top_score.text = "当前最高分：0"
        toast("已经重置最高分！")
    }

    private fun onClickExit(view: View) {
        finishAffinity()
        System.exit(0)
    }

    /**
     * 动态申请权限
     */
    fun handlePermisson(){

        // 需要动态申请的权限
        val permission = Manifest.permission.READ_EXTERNAL_STORAGE

        //查看是否已有权限
        val checkSelfPermission = ActivityCompat.checkSelfPermission(this,permission)

        if (checkSelfPermission  == PackageManager.PERMISSION_GRANTED) {
        }else{

            //没有拿到权限  是否需要在第二次请求权限的情况下
            // 先自定义弹框说明 同意后在请求系统权限(就是是否需要自定义DialogActivity)
            if(ActivityCompat.shouldShowRequestPermissionRationale(this,permission)){

                alert("安卓就是流氓的获取了你的私人信息","温馨提示"){

                    yesButton {
                        // 点击同意 请求真的权限
                        myRequestPermission()
                    }
                    noButton {
                        //可以回退到上一个界面 也可以不做任何处理
                    }
                }.show()

            }else{
                myRequestPermission()
            }
        }
    }

    private fun myRequestPermission() {
        //可以添加多个权限申请
        val permissions = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE)
        requestPermissions(permissions,1)
    }

    /***
     * 权限请求结果  在Activity 重新这个方法 得到获取权限的结果  可以返回多个结果
     */
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        //是否获取到权限
        if(grantResults[0] != PackageManager.PERMISSION_GRANTED){
            throw Exception("权限不允许")
        }
    }

    companion object {
        var _tv_top_score: TextView? = null

        fun setTopScore(topScore: Int) {
            _tv_top_score!!.text = "当前最高分：${topScore}"
        }
    }
}
